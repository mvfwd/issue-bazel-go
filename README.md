# Issue: Bazel + Go = could not resolve remote repository

## Links

- Building Go with Bazel could not resolve a repository
  - https://stackoverflow.com/questions/68826939/building-go-with-bazel-could-not-resolve-a-repository
- Building Go Services With Bazel
  - https://youtu.be/v7EAdff-YXQ?t=2093

## Error

```
$ bazel build ...
ERROR: /builds/mvfwd/issue-bazel-go/cmd/BUILD.bazel:3:11: no such package '@com_github_pallinder_go_randomdata//': The repository '@com_github_pallinder_go_randomdata' could not be resolved and referenced by '//cmd:cmd_lib'
ERROR: Analysis of target '//cmd:cmd' failed; build aborted: Analysis failed
INFO: Elapsed time: 1.263s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (1 packages loaded, 165 targets configured)
FAILED: Build did NOT complete successfully (1 packages loaded, 165 targets configured)
```

See the whole log: https://gitlab.com/mvfwd/issue-bazel-go/-/jobs/1512867123
