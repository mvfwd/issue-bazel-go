package main

import (
	"fmt"

	"github.com/Pallinder/go-randomdata"
)

func Hello() string {
	return "Hello"
}

func main() {
	fmt.Printf("%s, %s!", Hello(), randomdata.SillyName())
}
