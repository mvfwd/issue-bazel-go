package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHello(t *testing.T) {
	got := Hello()
	want := "Hello"

	if got != want {
		t.Error("got '%s' want '%s'", got, want)
	}

	assert.Equal(t, 123, 123, "they should be equal")
}
